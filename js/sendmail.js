/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

function dataValidate()
{
    var name = document.getElementById("txtName");
    //var mobile = document.getElementById("txtMobile");
    var email = document.getElementById("txtEmail");
    var comments = document.getElementById("txtComments");
   
    if(name.value=="" && email.value=="" && comments.value=="")
    {
        alert("Please fill in all fields!");
    }
    else
    {
	if(name.value=="")
        {
            alert("Please fill in your name!");
        }
       // else if(mobile.value=="")
	   // {
	   //  alert("Please fill in your mobile number!");
	   // }
        else if(email.value=="")
        {
            alert("Please fill in your email address!");
        }
        else if(comments.value=="")
        {
            alert("Please fill in your comments!");
        }
        else
        {
            document.form1.action = "sendmail.php";
            document.form1.submit();
        }
    }
}

function valEmail()
{
    var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    var address = document.getElementById("txtEmail").value;
    var status = new Boolean();
    if(reg.test(address) == false) 
    {
      alert('Invalid Email Address');
      document.getElementById("txtEmail").value="";
      document.getElementById("txtEmail").style.background="#c8c8c8";
      status = false;
    }
    else
    {
        document.getElementById("txtEmail").style.background="white";
        status = true;
    }
    return status;
}

//function valMobile()
//{
//    var phone = document.getElementById("txtMobile").value;
//    var phoneRegex = /^-?[0-9]+$/;
//    var status = new Boolean();
//    if(!phone.match( phoneRegex )) 
//    {
//        alert("Invalid phone number!");
//        document.getElementById("txtMobile").value="";
//        document.getElementById("txtMobile").style.background="#c8c8c8";
//        status = false;
//    }
//    else
//    {
//        document.getElementById("txtMobile").style.background="white";
//        status = true;
//    }
//    return status;
//}

function valName()
{
    var name = document.getElementById("txtName").value;
    var nameReg = /^([A-Za-z\s])+$/;
    var status = new Boolean();
        if(nameReg.test(name.trim())==false)
        {
            alert("Invalid name, don't put any number or symbol!");
            document.getElementById("txtName").value="";
            document.getElementById("txtName").style.background="#c8c8c8";
            status = false;
        }
        else
        {
            status = true;
            document.getElementById("txtName").style.background="white";
        }
    return status;
}

/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

function dataValidate()
{
    var title = document.getElementById("txtTitle");
	var name = document.getElementById("txtName");
    var email = document.getElementById("txtEmail");
	var product = document.getElementById("txtProduct");
    var serial = document.getElementById("txtSerial");
	var address = document.getElementById("txtAddress");
	var city = document.getElementById("txtCity");
	var postcode = document.getElementById("txtPostcode");
	var contact = document.getElementById("txtContact");
	var pdate = document.getElementById("txtPDate");
	var contractno = document.getElementById("txtContractNo");
	var location = document.getElementById("txtLocation");
   
    if(title.selected=="" && name.value=="" && email.value=="" && product.value=="" && serial.value=="" && address.value=="" && city.value=="" && postcode.value=="" && contact.value=="" && pdate.value=="" && contractno.value=="" && location.value=="")
    {
        alert("Please fill in all fields!");
    }
    else
    {
	if  (title.selected=="")
        {
            alert("Please choose a Title!");
        }
		
		else if(name.value=="")
        {
            alert("Please fill in your Name!");
        }
        else if(email.value=="")
        {
            alert("Please fill in your Email Address!");
        }
		else if(product.value=="")
        {
            alert("Please choose a Product!");
        }
        else if(serial.value=="")
        {
            alert("Please fill in your Serial Number!");
        }
		 else if(address.value=="")
        {
            alert("Please fill in your Mailing Address!");
        }
		 else if(city.value=="")
        {
            alert("Please fill in your City!");
        }
		 else if(postcode.value=="")
        {
            alert("Please fill in your Postcode!");
        }
		 else if(contact.value=="")
        {
            alert("Please fill in your Contact Number!");
        }
		 else if(pdate.value=="")
        {
            alert("Please fill in your Purchase Date!");
        }
		 else if(contractno.value=="")
        {
            alert("Please fill in your Contract Number!");
        }
		 else if(location.value=="")
        {
            alert("Please fill in your Purchase Location!");
        }
        else
        {
            document.form2.action = "warranty.php";
            document.form2.submit();
        }
    }
}

function valEmail()
{
    var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    var address = document.getElementById("txtEmail").value;
    var status = new Boolean();
    if(reg.test(address) == false) 
    {
      alert('Invalid Email Address');
      document.getElementById("txtEmail").value="";
      document.getElementById("txtEmail").style.background="#c8c8c8";
      status = false;
    }
    else
    {
        document.getElementById("txtEmail").style.background="white";
        status = true;
    }
    return status;
}

function valName()
{
    var name = document.getElementById("txtName").value;
    var nameReg = /^([A-Za-z\s])+$/;
    var status = new Boolean();
        if(nameReg.test(name.trim())==false)
        {
            alert("Invalid Name, don't put any number or symbol!");
            document.getElementById("txtName").value="";
            document.getElementById("txtName").style.background="#c8c8c8";
            status = false;
        }
        else
        {
            status = true;
            document.getElementById("txtName").style.background="white";
        }
    return status;
}

function valContact()
{
    var contact = document.getElementById("txtContact").value;
    var contactVal = /^-?[0-9]+$/;
    var status = new Boolean();
    if(!contact.match( contactVal )) 
    {
        alert("Invalid Contact Number!");
        document.getElementById("txtContact").value="";
        document.getElementById("txtContact").style.background="#c8c8c8";
        status = false;
    }
    else
    {
        document.getElementById("txtContact").style.background="white";
        status = true;
    }
    return status;
}

function valPostcode()
{
    var postcode = document.getElementById("txtPostcode").value;
    var postcodeVal = /^-?[0-9]+$/;
    var status = new Boolean();
    if(!postcode.match( postcodeVal )) 
    {
        alert("Invalid Postcode!");
        document.getElementById("txtPostcode").value="";
        document.getElementById("txtPostcode").style.background="#c8c8c8";
        status = false;
    }
    else
    {
        document.getElementById("txtPostcode").style.background="white";
        status = true;
    }
    return status;
}


